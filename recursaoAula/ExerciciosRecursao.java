import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 10032603
 */
public class ExerciciosRecursao {

    
   public static void main(String[] args) {
       Contagem.reset();
       System.out.printf("\nFatorial de 6 eh: %d\n", fatorial(6));
       System.out.println("Contador: " + Contagem.getCounter());
       
       
      Contagem.reset();
      System.out.println("\nSoma de 1 a 10: " + soma(1,10));
      System.out.println("Contador: " + Contagem.getCounter());

      Contagem.reset();       
      System.out.println("\nSoma de 1 a 2000: " + soma(1,2000));
      System.out.println("Contador: " + Contagem.getCounter());

      Contagem.reset();
      System.out.println("\nSoma de 10 a 15: " + soma(10,15));
      System.out.println("Contador: " + Contagem.getCounter());
      
      Contagem.reset();
      System.out.println("\nPow de 2^40: " + pow(2,40));
      System.out.println("Contador: " + Contagem.getCounter());

      Contagem.reset();
      System.out.println("\nPow de 2^40 dividindo: " + pow2(2,40));
      System.out.println("Contador: " + Contagem.getCounter());
      
      Contagem.reset();
      System.out.println("\nMult de 2*3: " + mult(2,5));
      System.out.println("Contador: " + Contagem.getCounter());
      
      
      Contagem.reset();
      System.out.println("\nMult de 2*3: " + mult(2,5));
      System.out.println("Contador: " + Contagem.getCounter());
            

      Contagem.reset();
      System.out.println("\nPalindrome A - ABJKIOkOIKJBA : " + palinA(new char[] {'A','B','J','K','I','O','k','O','I','K','J','B','A'}));
      System.out.println("Contador: " + Contagem.getCounter());
      
      Contagem.reset();
      System.out.println("\nPalindrome B - ABJKIOkOIKJBA : " + palinB("ABJKIOkOIKJBA"));
      System.out.println("Contador: " + Contagem.getCounter());
      

      Contagem.reset();
      System.out.println("\nrev hello world : " + rev(new char[] {'h','e','l','l','o',' ','w','o','r','l','d'}));
      System.out.println("Contador: " + Contagem.getCounter());
      
    }

   // 
   // lista de exercícios de recursão
   //

   /**
    * Exercicio 8 pow dividindo o resultado
    */
   public static long pow2 (int x, int n){
	   Contagem.add(); 
	   if (n ==0 ) return 1 ;
	   long result = pow2(x, n/2);
	   if (n % 2 == 0) return result * result;
	   return x * result * result;
   }
   

   /**
    * Exercicio 7 inverte palavras
    */
   public static char[] rev (char[] txt){
	   Contagem.add(); 
	   char ws = ' ';
	   int lastIndex = 0;
	   for (int i = 0; i < txt.length; i++ ){
		  if (txt[i] == ws){
			  txt = rev(txt, lastIndex, i);
			  lastIndex = i;
		  }
	   }
	   return txt;
   }

   public static char[] rev (char[] txt, int i, int j){
	   Contagem.add(); 
	  if (i > j) return txt;
	  char aux = txt[i];
	  txt[i] = txt[j];
	  txt[j] = aux;
	  return rev(txt, ++i, --j);
   }
   
   /**
    * Exercicio 6 Numero de uns - referencia binaria
    */
   public static int numerodeuns (int n){
	   Contagem.add(); 
	  if (n == 0) return 0;
	  if (n % 2 == 0) return numerodeuns (n/2);
	  return numerodeuns (n/2) + 1;
   }

   
   /**
    * Exercicio 5 palindrome A.
    */
   public static boolean palinA (char[] msg){
	  return  palinA(msg, 0,  msg.length - 1);
   }

   public static boolean palinA (char[] msg, int i, int j ){
	   Contagem.add(); 
	   if ( i >= j ) return true;
	   if ( msg[i] == msg [j]) return palinA(msg, ++i, --j); 
	   return false;
   }

   /**
    * Exercicio 5 palindrome B.
    */
   public static boolean palinB (String msg ){
	   Contagem.add(); 
	   if ( msg.length() <= 1 ) return true;
	   if ( msg.charAt(0) == msg.charAt(msg.length() - 1)) return palinB(msg.substring(1, msg.length() - 1));
	   return false;
   } 
   
   /**
    * Exercicio 4, calcular i*j sem mult
    * 
    */
   public static long mult (int i, int j){
	   Contagem.add();
	   if (i == 0) return 0;
	   return j + mult(--i, j);  
   }

   /**
    * Exercicio 3, calcular x^n
    * 
    * para n >= 0
    */
   public static long pow (int x, int n){
	   Contagem.add();
	   if (n == 0) return 1;
	   return x * pow (x, --n);
   }
   
   //
   // exercício 2
   //
   // pre: i>=j
   //
   public static long soma(int i, int j) {
       Contagem.add();
       if (i==j) return i;
       
       int meio = (i+j)/2;
       long esq = soma(i,meio);
       long dir = soma(meio+1, j);
       
       return esq + dir; 
      //   return soma(i,meio) + soma(meio+1, j); 
   }
   
   
   
   //
   // exercício 1
   // pré: n>=0
   //
   public static int fatorial(int n) {
        return fatorial(n,"");
   }
 
   public static int fatorial(int n, String espacos) {
       Contagem.add();      
       int resultado;
       System.out.println(espacos+"Fatorial de "+n);
         if (n==0) 
             resultado =1;
         else
            resultado = n*fatorial(n-1, espacos+"|   ");
         
         System.out.printf("%sResultado f(%d) = %d\n",
                espacos, n, resultado);
         return resultado;
   }
   
    
}
