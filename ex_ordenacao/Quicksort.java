void swap(int[] v, int i, int j)
{
  int aux = v[i];
  v[i] = v[j];
  v[j] = aux; 
}

void quick_sort(int[] v, int low, int high)
{
  int pivot;
  if ((high - low) > 0)
  {
    pivot = partition(v, low, high);
    quick_sort(v, low, pivot - 1);
    quick_sort(v, pivot + 1, high);
  }
}

int partition(int[] v, int low, int high)
{
  int i, p, firsthigh;
  firsthigh = low;
  p = high;
  for (i = low; i < high; i++)
  {
    if (v[i] < v[p])
    {
      swap(v, i, firsthigh);
      firsthigh = firsthigh + 1;
    }
  }
  swap(v, p, firsthigh);
  return firsthigh;
}

