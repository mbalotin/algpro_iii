import java.util.Random;

public class TestSort {

	
	
	public static void main(String[] args) {
	   

	   int[] V=null ;
	   int N=7, cont=0;
       if (args.length == 0) {
           System.out.println("usage: java TestSort (bubble | bublbeO | inser | selec | quick | merge | quickDual | quickDir) [N] [(cres|decr|aleat)] [debug==0|1]");
           return;
       }
	
       if (args.length > 1) {
           N = Integer.parseInt(args[1]);

       if (args.length > 2) {
              if (args[2].compareTo("cres")==0) 
        	      V = SortAlgos.inicializaCrescente(N);
   		      else if (args[2].compareTo("decr")==0) 
        	      V = SortAlgos.inicializaDecrescente(N);
   		      else //if (args[2].compareTo("aleat")==0) 
        	      V = SortAlgos.inicializaAleatorio(N);
           }
   		   else
        	  V = SortAlgos.inicializaAleatorio(N);
	   }
	   else V = SortAlgos.inicializaAleatorio(N);

		if (args.length > 3 && args[3].compareTo("0") == 0)
            SortAlgos.setDebug(false);
        else
          SortAlgos.setDebug(true); 
	

      //V = new int[]{ 9, 5, 4, 1, 3, 0, 8, 2, 7, 6};   
       SortAlgos.mostraVetor(V);
       Contagem.reset();

      if (args[0].compareTo("bubble")==0) 
          SortAlgos.bubbleSort1(V);
      else if (args[0].compareTo("bubbleO")==0) 
          SortAlgos.bubbleSort(V);
      else if (args[0].compareTo("selec")==0) 
          SortAlgos.selectionSort(V);
      else if (args[0].compareTo("inser")==0) 
          SortAlgos.insertionSort(V); 
      else if (args[0].compareTo("quickDir")==0) 
    	   SortAlgos.quickSort1(V, 0, V.length-1);
      else if (args[0].compareTo("quick")==0) 
    	   SortAlgos.quickSort(V, 0, V.length-1);
      else if (args[0].compareTo("quickDual")==0) 
          DualPivotQuicksort.sort(V);
      else
         System.out.println("\nMétodo inválido");
          
        
        
       SortAlgos.mostraVetor(V);
       System.out.println("Cont: "+Contagem.getCounter()+"\n");
   }

}
