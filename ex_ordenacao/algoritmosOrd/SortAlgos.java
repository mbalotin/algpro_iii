import java.util.Random;

public class SortAlgos {

    private static boolean debug;

    public static void setDebug(boolean d) { debug = d; }
    

   public static void bubbleSort1(int[] v) {
      
      int N = v.length;
 
      for (int i=0; i<N-1; i++) {
          for (int k=0; k<N-i-1; k++) {
               Contagem.add(1);
               if (v[k]>v[k+1]) {
                  int t = v[k];
                  v[k]=v[k+1];
                  v[k+1]=t;
              if (debug) mostraVetor(v);
               }
          }
       }

   }
    
   public static void bubbleSort(int[] v) {
      
      int N=v.length;
      boolean percorrernovamente = true;
      while (percorrernovamente) {
         percorrernovamente = false;
         for (int k=0; k<N-1; k++) {
             Contagem.add(1);
             if (v[k]>v[k+1]) {
                  int t = v[k];
                  v[k]=v[k+1];
                  v[k+1]=t;
                      percorrernovamente=true;
                    
                      if (debug) mostraVetor(v);
               
             }
         }
      }
    }

   public static void selectionSort(int[] v) {
    int N = v.length;
        int indmin=0;

        for (int i=0; i<N-1; i++) {
           indmin=i;
           for (int j=i+1; j<N; j++) {
               Contagem.add(1);
               if (v[j]<v[indmin]) {
                  indmin=j;
                               }
           }
           int temp = v[i];
           v[i] = v[indmin];
           v[indmin] = temp;
           if (debug) mostraVetor(v);
        }       
      
    }

   public static void insertionSort(int[] v) {
    int N = v.length;
       int aux;
       for (int i=1; i<N; i++) {
           aux = v[i];
           int j;
           for (j=i-1; j>=0; j--) {
               Contagem.add(1);
               if (v[j]>aux){
                  v[j+1] = v[j];
                }
               else break;
           }
           v[j+1]= aux;
           if (debug) mostraVetor(v);
        }
        
   }

    public static void quickSort(int arr[], int left, int right) {

        int index = partition(arr, left, right);
         
        if (debug) { 
            System.out.printf("após particionar: left: %d  right: %d  pos pivot: %d\n", left, right, index);
            mostraVetor(arr, 0, arr.length-1);
        }
        
          if (left < index - 1)
                quickSort(arr, left, index - 1);

          if (index < right)
                quickSort(arr, index, right);
          
    }
    
    private static int partition(int arr[], int left, int right)
    {
          int i = left, j = right;
          int tmp;

          int pivot = arr[(left + right) / 2];
              
          while (i <= j) {
                while (arr[i] < pivot)
                      i++;
                while (arr[j] > pivot)
                      j--;
                Contagem.add(1);
                if (i <= j) {
                      tmp = arr[i];
                      arr[i] = arr[j];
                      arr[j] = tmp;
                      i++;
                      j--;
                }
          }

          return i;
    }

    public static void quickSort1(int[] arr, int left, int right) {
        int index = partition1(arr, left, right);
        if (debug) { 
            System.out.printf("após particionar: left: %d  right: %d  pos pivot: %d\n", left, right, index);
            mostraVetor(arr, 0, arr.length-1);
        }
        
          if (left < index - 1)
                quickSort1(arr, left, index - 1);

          if (index < right)
                quickSort1(arr, index+1, right);
    }

    private static int partition1(int[] arr, int esq, int dir) {
        int tmp;

        //int pivotPos = (esq + dir) / 2;
         int pivotPos =  dir;
         
        int pivot = arr[ pivotPos ];

        tmp = arr[pivotPos];
        arr[pivotPos] = arr[dir];
        arr[dir] = tmp;

            Contagem.add(1);
        int index = esq; 

      for (int i = esq; i < dir; i++)
      {
         if ( (arr[i]) < pivot ) {
            //System.out.printf("trocou %d e %d\n",arr[index], arr[i] );
         
            Contagem.add(1);
            tmp = arr[index];
            arr[index] = arr[i];
            arr[i] = tmp;
            index++;
            }
      }

      tmp = arr[dir];
      arr[dir] = arr[index];
      arr[index] = tmp;
            Contagem.add(1);
      
      return index;
    }




    
    public static void mostraVetor(int[] v) {
        System.out.print("[");
//        for (int i=0; i < v.length; i++) System.out.printf("%3d ",v[i]);
        System.out.println("]");
    }

    public static void mostraVetor(int[] v, int l, int r) {
        System.out.print("[");
  //      for (int i=l; i <= r; i++) System.out.printf("%3d ",v[i]);
        System.out.println("]");
    }

   public static int[] inicializaAleatorio(int N) {
       Random rand = new Random();

        int[] v = new int[N];
        
        for (int i = 0; i< N; i++) v[i] = rand.nextInt(N*3);
        
        return v;       
    }

    public static int[] inicializaCrescente(int N) {
        int[] v = new int[N];
        
        for (int i = 0; i< N; i++) v[i] = i+1;
        
        return v;       
    }

    public static int[] inicializaDecrescente(int N) {
        int[] v = new int[N];
        
        for (int i = 0; i< N; i++) v[i] = N-i;
        
        return v;       
    }

    



}
