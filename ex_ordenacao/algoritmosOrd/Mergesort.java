void merge_sort(int [] v, int inicio, int fim)
{
  int meio;
  if (inicio < fim)
  {
    meio = (inicio+fim)/2;
    merge_sort(v, inicio, meio);
    merge_sort(v, meio+1, fim);
    merge(v, inicio, meio, fim);
  }
}

void merge(int [] v, int inicio, int meio, int fim)
{
  int nL = meio-inicio+1;
  int nR = fim-meio;

  int [] L = new int[nL];
  int [] R = new int[nR];

  System.arraycopy(v, inicio, L, 0, nL);
  System.arraycopy(v, meio+1, R, 0, nR);

  int iL = 0;
  int iR = 0;

  for (int k=inicio; k<=fim; k++)
  {
    if (iR < nR)
    {
      if (iL < nL)
      {
        if (L[iL] < R[iR])
          v[k] = L[iL++];
        else
          v[k] = R[iR++];
      }
      else
      {
        v[k] = R[iR++];
      }
    }
    else
    {
      v[k] = L[iL++];
    }
  }
}
