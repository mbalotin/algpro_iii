
import java.io.*;
import java.util.HashSet;
import java.util.*;
/**
 *
 * @author aa
 */
public class RevisorMap {

	private static Map<String,String> dictMap = new HashMap<String,String>();

   public static void main(String[] args) {

      if (args.length==0) {
          System.out.println("\nInforme nome do arquivo de entrada.\n");
          return;
      }
 
     long ti, tf;
     ti = System.currentTimeMillis();
 
     // Devemos iniciar lendo o dicionario do arquivo 'dicPT.txt' 
     // ... e armazenar em algum lugar...
     loadDict("dicPT.txt");
    
      // faltas do dicionario, as palavras abaixo devem ser adicionadas manualmente...
      dictMap.put("a","a");
	  dictMap.put("e","e");
      dictMap.put("o","o");
      dictMap.put("à","à");
      dictMap.put("á","á");
      dictMap.put("é","é");
      
	    tf = System.currentTimeMillis();

		  System.out.println("\nTempo para carregar o dicionário: " + ((tf- ti) / 1000.0) + " segundos\n");

		  System.out.println("\nLista de palavras não encontradas:\n");
  
       String linha = "";
       int numLinha = 0;
       ti = System.currentTimeMillis();
 
       // agora o revisor em si...
       //
       // dica
       // para retirar caracteres que não interessam:
		try {
             BufferedReader buff = new BufferedReader(new FileReader(args[0]));
             try {      
                   while((linha = buff.readLine()) != null) {		
				      	String limpo = linha.replaceAll("[^a-zA-Záéíóúàâêôãõüç\\s]", " ");
						String[] palavras = limpo.split(" ");
						for (int i = 0; i < palavras.length ;i++){
							if (!palavras[i].isEmpty() && !dictMap.containsKey(palavras[i].toLowerCase()))
								System.out.println("<"+palavras[i]+">");
						}
                   }
               } finally {
                           buff.close();
                          }
        } catch(IOException e){
                                e.printStackTrace();
                                }


                 

       tf = System.currentTimeMillis();
       System.out.println("\nTempo para verificação: " + ((tf- ti) / 1000.0) + " segundos\n");
    }


    public static void loadDict(String filename) {
       String linha;
       int cont=1;

       try {
             BufferedReader buff = new BufferedReader(new FileReader(filename));
             try {
                   // dicionário possui uma palavra por linha         
                   while((linha = buff.readLine()) != null) {
                        // como tem alguns nomes próprios em maiusculas vou unificar tudo para minusculas
                        // como não tenho estrutura de dados vou escrever na tela (pessima ideia, então apenas a cada 10000)
                        //if (cont++ % 10000 == 0)
                        //System.out.printf("entrada %d: %s\n", cont, linha.toLowerCase());
						dictMap.put(linha.toLowerCase(),linha.toLowerCase());
                   }
               } finally {
                           buff.close();
                          }
        } catch(IOException e){
                                e.printStackTrace();
                                }

    }

}
