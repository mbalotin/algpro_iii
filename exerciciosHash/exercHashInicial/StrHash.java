import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


/**
 * 
 * @author aa
 * 
 */
public class StrHash
{
	
	
	public static void main(String[] args)
	{
       String linha="";
       
	      for (char c='a'; c<'z';c++) {
             linha+=c;    
             System.out.printf("\n%26s \thash: %d", linha, linha.hashCode());          
       }
       System.out.println();       

        linha = "";
	      for (char c='a'; c<'z';c++) {
             linha = c + linha;    
             System.out.printf("\n%26s \thash: %d", linha, linha.hashCode());          
       }
       System.out.println();  
 
       linha = "abcdefghij";
	      for (char c='a'; c<='j';c++) {
             linha = linha.replace(String.valueOf(c),"a");    
             System.out.printf("\n%26s \thash: %d", linha, linha.hashCode());          
       }
       System.out.println();


   }
}
