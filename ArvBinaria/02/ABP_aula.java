import java.util.*;

public class ABP_aula  {
      private static final class Node {
            Node Left;
            Node Right;
            String Elem;
            
            public Node(String element) {
                  Left = null;
                  Right = null;
                  Elem = element;
            }      
      }

      private Node root;
      
      public ABP_aula() {
            root = null;
      }
      

      public void add(String valor) {
           root = this.add(root, new Node(valor));
      }

      private Node add(Node atual, Node novo) {
           
           if (atual==null) 
              atual = novo;
           else {
              if (novo.Elem.compareTo(atual.Elem) < 0)
                 atual.Left = add(atual.Left, novo);
              else
                  atual.Right = add(atual.Right, novo);
           }

           return atual;
      }


      public boolean isEmpty()
      {
            return (root == null);
      }
      
      public int count()
      {
            return count(root);
      }

      private int count(Node n)
      {
            if (n == null) 
                  return 0;
            else
                  return 1 + count(n.Left) + count(n.Right);
      }

        public int altura(){
          // if(isEmpty())
          //  throw new EmptyTreeException();

            // retorna -1 se this.isEmpty

            return altura(root);
        }

        private int altura(Node n) {
            if (n==null) return -1;
            
            return 1 + Math.max(altura(n.Left), altura(n.Right));
        }

        public List<String> Traversal_inOrder() {
            List<String> lista = new ArrayList<>();
            
            Traversal_inOrder(root, lista);

            return lista;
        }

       public String maior() {
           if(isEmpty())
              throw new EmptyTreeException();

            return maior(root).Elem;
       }

       private Node maior(Node n) {
          if (n.Right == null ) return n;      
          else
             return maior( n.Right );
          
       }

       public void Traversal_inOrder(Node n, List<String> lista) {
            
            if (n==null) return; 
            
            Traversal_inOrder(n.Left, lista);
            lista.add(n.Elem);
            Traversal_inOrder(n.Right, lista);
        }
	public boolean contains(String elem){
		return contains(root,elem);
	}
	private boolean contains(Node root, String elem){
		if (root == null) return false;
		if (root.Elem.equals(elem)) return true;
		if (contains(root.Left, elem)) return true;
		return contains(root.Right, elem);
	} 

	public String caminhoR(String elem){
		return caminhoR(root, elem);
	}
	private String caminhoR(Node root, String elem){
		if (root == null ) return null;
		if (root.Elem.equals(elem)) return root.Elem;
		String cam = caminhoR(root.Left, elem);
		if (cam != null) return root.Elem + " - " + cam;
		cam = caminhoR(root.Right, elem);
		if (cam != null) return root.Elem + " - " + cam;
		return null;
	}
	public String caminho(String elem1, String elem2){
		if (!contains(elem1) || !contains(elem2)) return null;		
		return caminho(root, elem1, elem2);
	}
	private String caminho (Node root,String elem1, String elem2){
		if (root == null) return null;
		if (root.Elem.equals(elem1)) return caminhoR(root, elem2);
		if (root.Elem.equals(elem2)) return caminhoR(root, elem1);
		String cam = caminho(root.Left, elem1, elem2);
		if (cam != null && !cam.isEmpty()) return cam;
		return caminho(root.Right, elem1, elem2);
	}
	
	public boolean  igualFormato(ABP_aula arvore){
		if (arvore == null) return false;
		return igualFormato(root, arvore.root);
	}
	private boolean igualFormato(Node a1, Node a2){
	
		if (a1 == null && a2 == null) return true;
		if (a1 == null || a2 == null) return false;
		return igualFormato(a1.Left, a2.Left) && igualFormato(a1.Right, a2.Right);
	
	} 
        public boolean  igual(ABP_aula arvore){
		if (arvore == null) return false;
                return igual(root, arvore.root);
        }
        private boolean igual(Node a1, Node a2){

                if (a1 == null && a2 == null) return true;
                if (a1 == null || a2 == null) return false;
                return a1.Elem.equals(a2.Elem) && igual(a1.Left, a2.Left) && igual(a1.Right, a2.Right);

        }

}

