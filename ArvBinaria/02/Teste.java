import java.util.*;

public class Teste {
      public static void main(String[] args) {
            ABP_aula arvore = new ABP_aula();

            System.out.println("Numero de elementos: " +arvore.count());
                
            System.out.println("Altura da arvore: " +arvore.altura());    

            try {
                  System.out.println("Maior Elemento: " +arvore.maior());
            } catch (EmptyTreeException e) {
                  System.out.println("Erro: árvore está vazia");
              }

            
            arvore.add("Huguinho");
            arvore.add("Zezinho");
            arvore.add("Luizinho");
            arvore.add("Donald");
            arvore.add("Patinhas");
            arvore.add("Margarida");          
            arvore.add("Clara"); 
            arvore.add("John"); 
            arvore.add("Smith"); 
            arvore.add("D'Ale"); 
		

	System.out.println("\nContem o elemento John ? "+arvore.contains("John") );         
	System.out.println("\nContem o elemento Marcio ? "+arvore.contains("Marcio") );         
	System.out.println("\nCaminho até D'Ale ? "+arvore.caminhoR("D'Ale") );         
	System.out.println("\nCaminho até Márcio ? "+arvore.caminhoR("Marcio") );         
	System.out.println("\nCaminho até Clara ? "+arvore.caminhoR("Clara") );         
	System.out.println("\nCaminho até Clara e D'Ale ? "+arvore.caminho("Clara","D'Ale") );         
	System.out.println("\nCaminho até D'Ale e Clara ? "+arvore.caminho("D'Ale","Clara") );         
	System.out.println("\narvore == arvore? "+arvore.igualFormato(arvore) );
	System.out.println("\narvore == null? "+arvore.igualFormato(null) );
	System.out.println("\narvore == Arvore? "+arvore.igual(arvore) );
         
            System.out.println("\nApós inserções... " );
              
            System.out.println("Numero de elementos: " +arvore.count());
                
            System.out.println("Altura da arvore: " +arvore.altura());
   
            System.out.println("Maior Elemento: " +arvore.maior());
                        

            List<String> valores = arvore.Traversal_inOrder();

            System.out.print("[ ");
            for(String str : valores)
                 System.out.print(str+" ");
           System.out.println("]");
              
            
      }
}
