import java.util.*;

public class ArvoreBinaria {

    private Node raiz;
	
	private static class Node {
        public String Elem;
        public Node Esq;
		public Node Dir;
		
		public Node(String e) {
			Elem = e;
			Esq = null;
			Dir = null;
		}
	}
	
	public ArvoreBinaria(String e) {
		raiz = new Node(e);
	}
	
	public boolean contains(String val) {
	     return searchNodeRef(raiz, val) != null;
	}

    private Node searchNodeRef(Node atual, String val) {
		if (atual == null) return null;
		if (atual.Elem.equals(val)) return atual;
		
		Node aux = searchNodeRef(atual.Esq, val);
		if (aux == null)
			aux = searchNodeRef(atual.Dir, val);
		return aux;	
	}
	
	public int altura() {
	    return altura(raiz);
	}
	
	public int altura(String val) {
	    return altura(searchNodeRef(raiz, val));
	}
	
	public int altura(Node atual) {
		if (atual == null) return -1;
		
		return 1 + Math.max(altura(atual.Esq), altura(atual.Dir));
	}
	
	private boolean insert(String val, String pai, ABinPosition pos) {
	    Node aux = searchNodeRef(raiz, pai);
		
		if (aux == null) return false;
		
		Node novo = new Node(val);
		if (pos == ABinPosition.ESQUERDA) {
		    novo.Esq = aux.Esq;
			aux.Esq = novo;
			}
		else {
			novo.Dir = aux.Dir;
			aux.Dir = novo;
		}

		return true;
	}

	private int contar(Node raiz){
		if (raiz == null)
			return 0;
		else
			return contar(raiz.Esq) + contar(raiz.Dir) + 1;
	}
	
	public int contar(){
		return contar(raiz);
	}

	public int grau(String val){
		Node aux = searchNodeRef(raiz, val);
		return grau(aux);	
	}
	private int grau(Node aux){
		if (aux == null)
			return -1 ;

		int result = 0;
		if (aux.Esq != null)
			result++;
		if (aux.Dir != null)
			result++;
		return result;
	}

	private int contarFolhas(Node raiz){
		if (raiz == null)
			return 0;
		else if ( grau(raiz) == 0)
			return 1;
		else
			return contarFolhas(raiz.Esq) + contarFolhas(raiz.Dir);	
	}

	public int contarFolhas(){
		return contarFolhas(raiz);
	}

	public String repsTextual(){
		return repsTextual(raiz);
	}


	private String repsTextual(Node raiz){
		if (raiz == null)
			return "^";
		else
			return "{"+raiz.Elem+","+repsTextual(raiz.Esq)+","+repsTextual(raiz.Dir)+"}";
	}

	public List<String> elementosNoNivel(int nivel){
		return 	elementosNoNivel(nivel, raiz);
	}

	private List<String> elementosNoNivel(int nivel, Node raiz){
		List<String> result = new ArrayList<String>();
		if (raiz != null)
			if (nivel == 0)
				result.add(raiz.Elem);
			else{
				result.addAll(elementosNoNivel(nivel -1, raiz.Esq));
				result.addAll(elementosNoNivel(nivel -1, raiz.Dir));
			}
		return result;
	}

    public static void main(String[] args) {

		ArvoreBinaria arv = new ArvoreBinaria("F");
		
		arv.insert("G", "F", ABinPosition.ESQUERDA);
		arv.insert("K", "F", ABinPosition.DIREITA);
		
	    arv.insert("A", "K", ABinPosition.ESQUERDA);
		arv.insert("C", "K", ABinPosition.DIREITA);
	
	    arv.insert("X", "A", ABinPosition.ESQUERDA);
		arv.insert("Y", "A", ABinPosition.DIREITA);

		arv.insert("P", "G", ABinPosition.ESQUERDA);
	
	    System.out.println("arv.contains(X): " + arv.contains("X")); 
		System.out.println("arv.contains(A): " + arv.contains("A")); 
		System.out.println("arv.contains(K): " + arv.contains("K")); 
		System.out.println("arv.contains(B): " + arv.contains("B")); 
		
		System.out.println("\naltura da arvore: " + arv.altura()); 
		System.out.println("\naltura(F): " + arv.altura("F")); 
		System.out.println("altura(G): " + arv.altura("G")); 
		System.out.println("altura(K): " + arv.altura("K")); 
		System.out.println("altura(A): " + arv.altura("A")); 
		System.out.println("altura(C): " + arv.altura("C"));
		System.out.println("altura(X): " + arv.altura("X"));
		System.out.println("altura(Y): " + arv.altura("Y"));
		System.out.println("altura(Z): " + arv.altura("Z"));
		
		System.out.println("Número de elementos: " + arv.contar());

		System.out.println("Número de folhas: " + arv.contarFolhas());
		System.out.println("grau(A): " + arv.grau("A"));
		System.out.println("grau(X): " + arv.grau("X"));
		System.out.println("grau(G): " + arv.grau("G"));

		System.out.println("A: " + arv.repsTextual());

		System.out.println("elementos no nível: 0 " + arv.elementosNoNivel(0));
		System.out.println("elementos no nível: 1 " + arv.elementosNoNivel(1));
		System.out.println("elementos no nível: 2 " + arv.elementosNoNivel(2));
		System.out.println("elementos no nível: 3 " + arv.elementosNoNivel(3));
	
		
	 }   
}	

