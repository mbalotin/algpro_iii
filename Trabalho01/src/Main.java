import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
	
		if (args.length != 2){
			System.out.println("Informe os paramenttros \n  <arquivo de entrada> <arquivo de saida>");
			return;
		}
		BufferedReader buffer = null;
		try {
			buffer = new BufferedReader(new FileReader(args[0]));
		} catch (FileNotFoundException e1) {
			System.out.println("Arquivo "+args[0]+" nao encontrado.");
			return;
		}
		String input = null;
		try{
			input = buffer.readLine();
		} catch (Exception e){
			System.out.println("Nao foi possivel ler a linha. \n" + e.getMessage());
			return;
		} finally {
			try {
				buffer.close();
			} catch (IOException e) {}
		}

		int size = 0;
		char[] imageInput = null;
		try{
			size = Integer.valueOf(input.split(" ")[0]);
		}catch (Exception ex){
			System.out.println("Valor de tamanho invalido. \n" + input.split(" ")[0]);
			return;
		}
		try{
			imageInput = input.split(" ")[1].toCharArray();
		}catch (Exception ex){
			System.out.println(ex.getMessage());
			return;
		}
		
		Decriptor decr = new Decriptor();
		try {
			System.out.printf("Imagem gerada, %d pontos pretos.\n", decr.decript(imageInput, size, args[1]));
			System.out.printf("Decriptação finalizada, arquivo \"" + args[1] + "\" gerado.");
		} catch (IOException e) {
			System.out.println("Erro ao gerar arquivo de saida. \n" + e.getMessage());
		}
	}
		
}
