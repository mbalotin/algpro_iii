import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Decriptor {
	
	private int index;
	
	/**
	 * @param input incripted Array
	 * @param size Image size
	 * @param svgName File name to generate the .svg file
	 * @return value of black pixels on the image.
	 * @throws IOException If unable to generate the .svg file.
	 */
	public int decript(char[] input, int size, String svgName) throws IOException {
		index = 0;
		return generateFile(buildTree(input), size, svgName);
	}

	private  Node buildTree(char[] input) throws IndexOutOfBoundsException {
		Node node = new Node(input[index], index++);
		if (node.value == 'c'){
			node.q1 = buildTree(input);
			node.q2 = buildTree(input);
			node.q3 = buildTree(input);
			node.q4 = buildTree(input);
		}
		return node;
	}

	private int generateFile(Node root, int size, String fileName) throws IOException {
		File file = new File(fileName);
		BufferedWriter writer =  null;
		try {
			writer = new BufferedWriter(new FileWriter(file));
		} catch (IOException e) {
			System.out.println("NAO foi possivel salvar no arquivo "+fileName);
			throw e;
		}
		writer.write("<!DOCTYPE html>");
		writer.write("<html> <body> <svg xmlns=\"http://www.w3.org/2000/svg\" width=\""+size+"\" height=\""+size+"\">"+ System.lineSeparator());
		int count = printTreeComponents(root, size, writer, 0, 0);
		writer.write("</svg> </body> </html>");
		writer.flush();
		writer.close();
		return count;
	}
	
	private int printTreeComponents(Node root,
			int size,
			BufferedWriter writer,
			int x,
			int y)
					throws IOException {
		if (root == null) return 0;
		int count = 0;
		if ('p' == root.value ){
			writer.write("<rect x=\""+(x)+"\" y=\""+(y)+"\" width=\""+size+"\" height=\""+size+"\" style=\"fill:#fffff;\"/>" + System.lineSeparator());
			count += size * size ;
		}
		size = size/2;
		count += printTreeComponents(root.q1, size, writer, size + x, y);
		count += printTreeComponents(root.q2, size, writer, x, y);
		count += printTreeComponents(root.q3, size, writer, x, size + y);
		count += printTreeComponents(root.q4, size, writer, size + x, size + y);
		return count;
	}
}
