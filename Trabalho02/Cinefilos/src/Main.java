
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Main {

	public static final String ACTORS_FILE = "Atores.txt";
	public static final String FILMS_FILE = "Filmes.txt";
	public static final String EDGES_FILE = "FilmesAtores.txt";

	private static final Map<Integer, Actor> actors = new HashMap<Integer, Actor>();
	private static final Map<Integer, Film> films = new HashMap<Integer, Film>();
	private static final String KEVIN_BACON_NAME = "Kevin Bacon";
	private static Actor KEVIN_BACON;

	public static void main(String[] args) {
		try {
			loadActors();
			loadFilms();
			loadEdges();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.printf("Exception: %s\n", e.toString());
			return;
		}
		System.out.printf("LOADED all actors and films.\n");

		for (Iterator<Film> iterator = films.values().iterator(); iterator.hasNext();) {
			Film film = iterator.next();
			if (film.actors.isEmpty()) {
				iterator.remove();
			} else {
				for (Actor actor : film.actors) {
					for (Actor actedWith : film.actors) {
						actor.actedWith(actedWith, film);
					}
				}
			}
		}

		System.out.println("Os Atores mais distantes de Kevin Bacon sao:");
		for (Entry<Actor, List<Actor>> actor : KEVIN_BACON.farthestActors().entrySet())
			System.out.printf("    %d %s com distancia %d.\n", actor.getKey().id, actor.getKey().name,
					actor.getValue().size());

		System.out.printf("\n\n%d atores com numero de Bacon igual a %d.\n",
				KEVIN_BACON.actorsFarWithDistance(1).size(), 1);
		System.out.printf("%d atores com numero de Bacon %d.\n", KEVIN_BACON.actorsFarWithDistance(2).size(), 2);
		System.out.printf("%d atores com numero de Bacon %d.\n", KEVIN_BACON.actorsFarWithDistance(3).size(), 3);
		System.out.printf("%d atores com numero de Bacon %d.\n", KEVIN_BACON.actorsFarWithDistance(4).size(), 4);
		System.out.printf("%d atores com numero de Bacon %d.\n", KEVIN_BACON.actorsFarWithDistance(5).size(), 5);
		System.out.printf("%d atores com numero de Bacon %d.\n", KEVIN_BACON.actorsFarWithDistance(6).size(), 6);
		System.out.printf("%d atores com numero de Bacon %d.\n", KEVIN_BACON.actorsFarWithDistance(7).size(), 7);
		System.out.printf("%d atores com numero de Bacon %d.\n", KEVIN_BACON.actorsFarWithDistance(8).size(), 8);
		System.out.printf("%d atores com numero de Bacon %d.\n\n", KEVIN_BACON.actorsFarWithDistance(9).size(), 9);
	
		printPathBetweenActors(KEVIN_BACON, actors.get(3));
		printPathBetweenActors(KEVIN_BACON, actors.get(2));
		printPathBetweenActors(KEVIN_BACON, actors.get(7689));
		printPathBetweenActors(KEVIN_BACON, actors.get(6689));
		printPathBetweenActors(KEVIN_BACON, actors.get(5678));
		printPathBetweenActors(KEVIN_BACON, actors.get(2524));
		printPathBetweenActors(KEVIN_BACON, actors.get(4241));
		printPathBetweenActors(KEVIN_BACON, actors.get(4240));

		printPathBetweenActors(actors.get(7689), actors.get(5678));
		printPathBetweenActors(actors.get(1150), actors.get(4240));
		printPathBetweenActors(actors.get(3350), actors.get(6240));
		printPathBetweenActors(actors.get(7350), actors.get(2240));
		printPathBetweenActors(actors.get(4240), actors.get(6689));
		printPathBetweenActors(actors.get(5678), actors.get(7));

	}

	private static void printPathBetweenActors(Actor from, Actor to) {
		if (from.distanceTo(to) < 0) {
			System.out.printf("%s and %s are not connected, there is no available path.\n", from.name, to.name);
			return;
		} else {
			System.out.printf("Distance from %s to %s: %d\n", from.name, to.name, from.distanceTo(to));
		}
		BufferedWriter writer = null;
		try {
			String filename = "path" + String.valueOf(from.id) + "To" + String.valueOf(to.id);
			File file = new File(filename + ".dot");
			writer = new BufferedWriter(new FileWriter(file));
			writer.write(from.pathToWithDotLanguage(to));
			try {
				Runtime.getRuntime().exec("xterm -e dot -Tpng \"" + file.getAbsolutePath() + "\" > \""
						+ file.getAbsolutePath().replace(".dot", ".png") + "\"");
			} catch (IOException ex) {
				try {
					Runtime.getRuntime().exec("cmd.exe /C dot -Tpng \"" + file.getAbsolutePath() + "\" > \""
							+ file.getAbsolutePath().replace(".dot", ".png") + "\"");
				} catch (IOException ex2) {
					System.out.printf(
							"       Erro ao gerar aquivo .png, %s.dot criado com sucesso, graphviz precisa ser executado manualemnte.\n",
							filename, filename);
					return;
				}
			}
			System.out.printf("       Graphviz %s.dot and %s.png created.\n", filename, filename);
		} catch (Exception e) {
			System.out.println("Nao foi possivel gerar o arquivo de saida.");
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (Exception e) {
			}
		}
	}

	private static void loadEdges() throws IOException {
		BufferedReader reader = null;
		reader = new BufferedReader(new FileReader(new File(EDGES_FILE)));
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				String[] entry = line.split("\\|");
				int filmId = Integer.valueOf(entry[0]);
				int actorId = Integer.valueOf(entry[1]);
				films.get(filmId).actors.add(actors.get(actorId));
			}
		} finally {
			try {
				reader.close();
			} catch (Exception e) {
			}
		}
	}

	private static void loadFilms() throws IOException {
		BufferedReader reader = null;
		reader = new BufferedReader(new FileReader(new File(FILMS_FILE)));
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				String[] entry = line.split("\\|");
				int id = Integer.valueOf(entry[0]);
				films.put(id, new Film(id, entry[1]));
			}
		} finally {
			try {
				reader.close();
			} catch (Exception e) {
			}
		}
	}

	private static void loadActors() throws Exception {
		BufferedReader reader = null;
		reader = new BufferedReader(new FileReader(new File(ACTORS_FILE)));
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				String[] entry = line.split("\\|");
				int id = Integer.valueOf(entry[0]);
				Actor newActor = new Actor(id, entry[1]);
				if (KEVIN_BACON_NAME.equals(newActor.name))
					KEVIN_BACON = newActor;
				actors.put(id, newActor);
			}
			if (KEVIN_BACON == null) {
				throw new NullBaconException();
			}
		} finally {
			try {
				reader.close();
			} catch (Exception e) {
			}
		}
	}
}
