
public class NullBaconException extends Exception {
	public NullBaconException() {
		super("No Kevin Bacon Found.");
	}
}
