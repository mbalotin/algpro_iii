
import java.util.HashSet;
import java.util.Set;

public class Film {
	int id;
	String title;
	Set<Actor> actors;
	
	public Film(int id, String title) {
		this.id = id;
		this.title = title;
		actors = new HashSet<Actor>();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Film)
			return this.id == ((Film) obj).id;
		return false;
	}

	@Override
	public int hashCode() {
		return this.id;
	}
	
}
