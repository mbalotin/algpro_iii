
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Random;

public class Actor {

	String name;
	int id;
	Map<Actor, Edge> adj;

	private Map<Actor, List<Actor>> distances;

	private Map<Actor, List<Actor>> getDistances() {
		if (distances == null)
			dijkstra();
		return distances;
	}

	public Actor(int id, String name) {
		this.name = name;
		this.id = id;
		adj = new HashMap<Actor, Edge>();
	}

	public void actedWith(Actor with, Film on) {
		if (!equals(with)) {
			Edge edge = adj.get(with);
			if (edge == null) {
				edge = new Edge();
				this.adj.put(with, edge);
				with.adj.put(this, edge);
			}
			edge.films.add(on);
		}
	}

	public String pathToWithDotLanguage(Actor actor) {
		String result = "";
		if (getDistances().containsKey(actor)) {
			result += "graph { \"" + this.name + "\"";
			String filmsNodeCode = "";
			List<Actor> path = getDistances().get(actor);
			Actor lastActor = this;
			Random r = new Random();
			for (Actor step : path) {
				Edge edge = lastActor.adj.get(step);
				String filmTitle = edge.films.get(r.nextInt(edge.films.size())).title;

				result += " -- \"" + filmTitle + "\" -- \"" + step.name + "\"";
				filmsNodeCode += "\"" + filmTitle + "\" [shape=box  color=\"#7c821e\" style=filled ] ";
				lastActor = step;
			}
			result += ";" + filmsNodeCode + "}";
		}
		return result;
	}

	public int distanceTo(Actor actor) {
		if (getDistances().containsKey(actor))
			return getDistances().get(actor).size();
		return -1;
	}

	public Map<Actor, List<Actor>> farthestActors() {
		Map<Actor, List<Actor>> result = new HashMap<Actor, List<Actor>>();
		int farthest = -1;
		for (Entry<Actor, List<Actor>> entry : getDistances().entrySet()) {
			if (entry.getValue().size() >= farthest) {
				if (entry.getValue().size() > farthest) {
					result.clear();
					farthest = entry.getValue().size();
				}
				result.put(entry.getKey(), entry.getValue());
			}
		}
		return result;
	}

	public Map<Actor, List<Actor>> actorsFarWithDistance(int distance) {
		if (distance <= 0)
			throw new InvalidParameterException("Distance must be greatter than 0.");
		Map<Actor, List<Actor>> result = new HashMap<Actor, List<Actor>>();
		for (Entry<Actor, List<Actor>> entry : getDistances().entrySet())
			if (entry.getValue().size() == distance)
				result.put(entry.getKey(), entry.getValue());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Actor)
			return this.id == ((Actor) obj).id;
		return false;
	}

	@Override
	public int hashCode() {
		return this.id;
	}

	@Override
	public String toString() {
		return name + "/" + id;
	}

	private void dijkstra() {
		distances = new HashMap<Actor, List<Actor>>();

		PriorityQueue<Actor> queue = new PriorityQueue<Actor>(new Comparator<Actor>() {
			@Override
			public int compare(Actor o1, Actor o2) {
				int dist1 = Integer.MAX_VALUE;
				int dist2 = Integer.MAX_VALUE;
				if (distances.containsKey(o1))
					distances.get(o1).size();
				if (distances.containsKey(o2))
					distances.get(o2).size();
				return Integer.compare(dist2 , dist1 );
			}
		});
		queue.add(this);
		distances.put(this, new ArrayList<Actor>());
		while (!queue.isEmpty()) {
			Actor actor = queue.poll();
			for (Actor adj : actor.adj.keySet()) {
				int distance = Integer.MAX_VALUE;
				if (distances.containsKey(adj))
					distance = distances.get(adj).size();
				if (distance > distances.get(actor).size() + 1) {
					List<Actor> set = new ArrayList<Actor>();
					set.addAll(distances.get(actor));
					set.add(adj);
					distances.put(adj, set);
					queue.add(adj);
				}

			}

		}
	}
}
